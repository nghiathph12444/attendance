package com.demo_a_c.API;


import com.demo_a_c.Model.Attendance;
import com.demo_a_c.Model.ChangePass;
import com.demo_a_c.Model.ChangePassForgot;
import com.demo_a_c.Model.GetList;
import com.demo_a_c.Model.GetMassage;
import com.demo_a_c.Model.Located;
import com.demo_a_c.Model.LoginResponse;
import com.demo_a_c.Model.SendOTP;
import com.demo_a_c.Model.UserLoginRequest;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;

import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {
    @POST("v1/auth/signin")
    Call<LoginResponse> loginRequest(@Body UserLoginRequest loginRequest);

    @POST("v1/auth/send-otp")
    Call<GetMassage> getOTP(@Body SendOTP sendOTP);

    @POST("v1/auth/change-pass-forgot")
    Call<GetMassage> changeMassage(@Body ChangePassForgot changePassForgot);

    @POST("v1/user/change-password")
    Call<GetMassage> changePass( @Body ChangePass changePass);


    //loadmore
    @GET("v1/attendance/get-attendances")
    Call<GetList> getList(@Query("page") int currentPage);

    @POST("v1/attendance/attendance-today")
    Call<GetMassage> attendance(@Body Located located);

    @GET("v1/user/get-my-profile")
    Call<LoginResponse> getProfile();

}
