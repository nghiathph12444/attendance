package com.demo_a_c.API;

public class BaseObject {
    String message;
    Object object;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public BaseObject() {
    }

    public BaseObject(String message, Object object) {
        this.message = message;
        this.object = object;
    }
}
