package com.demo_a_c.API;

import static okhttp3.internal.Util.format;

import android.content.SharedPreferences;
import android.text.TextUtils;
import com.demo_a_c.Login.LoginActivity;
import com.demo_a_c.Splat.SplatActivity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit retrofit;
    private static String BASE_URL = "http://65.108.149.39:8082/api/";

    public static Retrofit getRetrofit() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();


        if (SplatActivity.checkToken!=""){
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .addInterceptor(chain -> {
                        Request request = chain.request();
                        Request newReq = request
                                .newBuilder()
                                .addHeader("Content-Type", "application/json; charset=utf-8")
                                .addHeader("Authorization", "Bearer "+ SplatActivity.checkToken)
                                .build();
                        return chain.proceed(newReq);
                    })
                    .build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        else {
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .addInterceptor(chain -> {
                        Request request = chain.request();
                        Request newReq = request
                                .newBuilder()
                                .addHeader("Content-Type", "application/json; charset=utf-8")
                                .addHeader("Authorization", "Bearer "+ LoginActivity.token)
                                .build();
                        return chain.proceed(newReq);
                    })
                    .build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }







        return retrofit;
    }
}
