package com.demo_a_c.BroadCast;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.demo_a_c.R;
import com.google.android.material.bottomsheet.BottomSheetDialog;



public class MyBroadCastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (isOnline(context)) {
            BottomSheetDialog dialog = new BottomSheetDialog(context);
            dialog.setContentView(R.layout.dialog_connect);
            TextView tv_check_connect = dialog.findViewById(R.id.tv_check_connect);
            tv_check_connect.setText("Khôi phục kết nối");
            dialog.show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                }
            }, 2000);
            Toast.makeText(context, "Online", Toast.LENGTH_SHORT).show();
            Log.d("asd", "online: ");
        } else {
            BottomSheetDialog dialog = new BottomSheetDialog(context);
            dialog.setContentView(R.layout.dialog_connect);
            TextView tv_check_connect = dialog.findViewById(R.id.tv_check_connect);
            Handler handler = new Handler();
            tv_check_connect.setText("Kiểm tra két nối");
            dialog.show();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.dismiss();
                }
            }, 2000);
            Toast.makeText(context, "Offline", Toast.LENGTH_SHORT).show();
            Log.d("asd", "offline: ");
        }
    }

    public boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }

}
