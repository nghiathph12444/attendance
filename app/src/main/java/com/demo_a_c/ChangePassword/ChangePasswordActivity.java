package com.demo_a_c.ChangePassword;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.demo_a_c.API.ApiService;
import com.demo_a_c.API.RetrofitClient;
import com.demo_a_c.Model.ChangePass;
import com.demo_a_c.Model.GetMassage;
import com.demo_a_c.R;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {
    ImageView back;
    EditText edtOldPass, edtNewPass, edtReNewPass;
    Button btnSetpass;
    TextInputLayout tilOld, tilNewPass, tilReNewpPass;
//    private BroadcastReceiver broadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);

        anhXa();
        checkInput();
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnSetpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPass();
            }
        });
    }

    private void setPass(){
         if (edtReNewPass.getText().toString().equals("")
            && edtNewPass.getText().toString().equals("")
            && edtOldPass.getText().toString().equals("")){
            tilOld.setHelperText("*Nhập mật khẩu cũ");
            tilNewPass.setHelperText("*Nhập mật khẩu mới");
            tilReNewpPass.setHelperText("*Nhập lại xác nhận mật khẩu");
        }
        else if (edtReNewPass.getText().toString().equals("")){
             if (edtReNewPass.getText().toString().equals("")){
                 tilReNewpPass.setHelperText("*Nhập xác nhận lại mật khẩu");
             }
        }
        else if (edtOldPass.getText().toString().equals("")){
             tilOld.setHelperText("*Nhập mật khẩu cũ");
        }
        else if (edtNewPass.getText().toString().equals("")){
             tilNewPass.setHelperText("*Nhập mật khẩu mới");
        }
        if (!edtReNewPass.getText().toString().equals(edtNewPass.getText().toString())){
            tilReNewpPass.setHelperText("*Không trùng mật khẩu mới");
        }
        else {
            ChangePass changePass = new ChangePass(edtReNewPass.getText().toString(), edtOldPass.getText().toString());
            ApiService apiService = RetrofitClient.getRetrofit().create(ApiService.class);
            Call<GetMassage> call = apiService.changePass(changePass) ;
            call.enqueue(new Callback<GetMassage>() {
                @Override
                public void onResponse(Call<GetMassage> call, Response<GetMassage> response) {
                    if (response.body()==null){
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(getBaseContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("err", "err: "+ e);
                        } catch (IOException e) {
                            e.printStackTrace();
                            Log.d("err", "errs: "+ e);
                        }
                    }
                    else {
                        Toast.makeText(getBaseContext(),response.body().getMessage(), Toast.LENGTH_LONG).show();
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<GetMassage> call, Throwable t) {
                    Toast.makeText(getBaseContext(), "Fail to connect", Toast.LENGTH_LONG).show();
                }
            });
        }
    }
    private  void checkInput(){
        edtOldPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtOldPass.getText().toString().equals("")){
                    tilOld.setHelperText("*Nhập mật khẩu cũ");
                }else tilOld.setHelperText("");
            }
        });
        edtNewPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtNewPass.getText().toString().equals("")){
                    tilNewPass.setHelperText("*Nhập mật khẩu mới");
                }
                else tilNewPass.setHelperText("");
            }
        });
        edtReNewPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtReNewPass.getText().toString().equals("")){
                    tilReNewpPass.setHelperText("*Nhập xác nhận lại mật khẩu");
                }
                else if (!edtReNewPass.getText().toString().equals(edtNewPass.getText().toString())){
                    tilReNewpPass.setHelperText("*Không trùng mật khẩu mới");
                }
                else
                    tilReNewpPass.setHelperText("");

            }
        });
    }
    private void anhXa(){
        back = findViewById(R.id.back);
        edtOldPass = findViewById(R.id.edtOldPass);
        edtNewPass = findViewById(R.id.edtNewPass);
        edtReNewPass = findViewById(R.id.edtChangeRePass);
        btnSetpass = findViewById(R.id.btnSetPassword);
        tilOld = findViewById(R.id.til_oldPass);
        tilNewPass = findViewById(R.id.til_newpass);
        tilReNewpPass = findViewById(R.id.til_renewpass);
    }
}