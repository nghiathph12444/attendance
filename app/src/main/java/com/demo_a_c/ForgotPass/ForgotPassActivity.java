package com.demo_a_c.ForgotPass;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.demo_a_c.API.ApiService;
import com.demo_a_c.API.RetrofitClient;
import com.demo_a_c.Login.LoginActivity;
import com.demo_a_c.Model.GetMassage;
import com.demo_a_c.Model.SendOTP;
import com.demo_a_c.R;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassActivity extends AppCompatActivity {
    Button btn;
    ImageView back;
    TextView edt_sdt;
    public static String email,device_key;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);
        btn =findViewById(R.id.btn_getPass);
        back = findViewById(R.id.back);
        edt_sdt = findViewById(R.id.edt_sdt);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(Color.WHITE);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPassword();
                Log.d("zxcv", "log: "+ email + "_" +device_key);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    public void getPassword(){
        ApiService apiService = RetrofitClient.getRetrofit().create(ApiService.class);
        email = edt_sdt.getText().toString();
        device_key = Settings.Secure.getString(getBaseContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        SendOTP sendOTP = new SendOTP( email,device_key);
        Call<GetMassage> call = apiService.getOTP(sendOTP);
        call.enqueue(new Callback<GetMassage>() {
            @Override
            public void onResponse(Call<GetMassage> call, Response<GetMassage> response) {
                if (response.body() == null){
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getBaseContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    Toast.makeText(getBaseContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(ForgotPassActivity.this, OTPActivity.class);
                    intent.putExtra("email", email);
                    intent.putExtra("key", device_key);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<GetMassage> call, Throwable t) {
                Log.d("acb", "fail: " +t);
            }
        });
    }
}