package com.demo_a_c.ForgotPass;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.demo_a_c.API.ApiService;
import com.demo_a_c.API.RetrofitClient;
import com.demo_a_c.Login.LoginActivity;
import com.demo_a_c.Model.ChangePassForgot;
import com.demo_a_c.Model.GetMassage;
import com.demo_a_c.Model.SendOTP;
import com.demo_a_c.R;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPActivity extends AppCompatActivity {
    ImageView back;
    EditText edtOTP, edtNewpass, edtReNewpass;
    TextInputLayout tilOTP, tilNewpass, tilReNewpass;
    Button btnGetPass;
    TextView tvSendOTPAgain;
    Bundle bundle;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        anhXa();
        checkInput();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(Color.WHITE);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btnGetPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPass();
            }
        });
        tvSendOTPAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getOTP();
            }
        });

    }

    private void setPass() {
        if (edtOTP.getText().toString().equals("")
                && edtReNewpass.getText().toString().equals("")
                && edtNewpass.getText().toString().equals("")) {
            tilOTP.setHelperText("*OTP đang bỏ trống");
            tilNewpass.setHelperText("*Mật khẩu mới đang bỏ trống");
            tilReNewpass.setHelperText("*Nhập lại mật khẩu không được bỏ trống");
        }else if (edtOTP.getText().toString().equals("")) {
            tilOTP.setHelperText("*OTP đang bỏ trống");
        }
        else if (edtNewpass.getText().toString().equals("")) {
            tilNewpass.setHelperText("*Mật khẩu mới đang bỏ trống");
        } else if (edtReNewpass.getText().toString().equals("")) {
            tilReNewpass.setHelperText("*Nhập lại mật khẩu đang bỏ trống");
        }  else if (!edtReNewpass.getText().toString().equals(edtNewpass.getText().toString())) {
            tilReNewpass.setHelperText("*Không trung với mật khẩu đã nhập");
        } else {
            ApiService apiService = RetrofitClient.getRetrofit().create(ApiService.class);
            ChangePassForgot changePassForgot = new ChangePassForgot(ForgotPassActivity.device_key,
                    edtOTP.getText().toString(),
                    edtNewpass.getText().toString(),
                    ForgotPassActivity.email
            );
            Call<GetMassage> call = apiService.changeMassage(changePassForgot);
            call.enqueue(new Callback<GetMassage>() {
                @Override
                public void onResponse(Call<GetMassage> call, Response<GetMassage> response) {
                    if (response.body() == null) {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(getBaseContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getBaseContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                        startActivity(new Intent(getBaseContext(), LoginActivity.class));
                        finishAffinity();
                    }
                }

                @Override
                public void onFailure(Call<GetMassage> call, Throwable t) {

                }
            });
        }
    }

    private void anhXa() {
        back = findViewById(R.id.back);
        edtOTP = findViewById(R.id.edtOTP);
        edtNewpass = findViewById(R.id.edtNewpass);
        edtReNewpass = findViewById(R.id.edtReNewpass);
        tilOTP = findViewById(R.id.til_oldPass);
        tilNewpass = findViewById(R.id.til_newpass);
        tilReNewpass = findViewById(R.id.til_renewpass);
        btnGetPass = findViewById(R.id.btn_getPass);
        tvSendOTPAgain = findViewById(R.id.tvSendAgain);
        intent = getIntent();
        bundle = intent.getExtras();
    }

    private void checkInput() {
        edtOTP.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtOTP.getText().toString().equals("")) {
                    tilOTP.setHelperText("*OTP đang bỏ trống");
                } else tilOTP.setHelperText("");
            }
        });
        edtNewpass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtNewpass.getText().toString().equals("")) {
                    tilNewpass.setHelperText("*Mật khẩu mới đang bỏ trống");
                } else tilNewpass.setHelperText("");
            }
        });
        edtReNewpass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!edtReNewpass.getText().toString().equals(edtNewpass.getText().toString())) {
                    tilReNewpass.setHelperText("*Không trung với mật khẩu đã nhập");
                } else tilReNewpass.setHelperText("");
            }
        });
    }

    public void getOTP() {
        ApiService apiService = RetrofitClient.getRetrofit().create(ApiService.class);

        SendOTP sendOTP = new SendOTP(bundle.getString("email"), bundle.getString("key"));
        Call<GetMassage> call = apiService.getOTP(sendOTP);
        call.enqueue(new Callback<GetMassage>() {
            @Override
            public void onResponse(Call<GetMassage> call, Response<GetMassage> response) {
                if (response.body() == null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(getBaseContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getBaseContext(), "Mã xác nhận đã được gửi lại vào email của bạn", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<GetMassage> call, Throwable t) {
                Log.d("acb", "fail: " + t);
            }
        });
    }
}