package com.demo_a_c.Information;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.demo_a_c.API.ApiService;
import com.demo_a_c.API.RetrofitClient;
import com.demo_a_c.Login.LoginActivity;
import com.demo_a_c.Model.LoginResponse;
import com.demo_a_c.Page.HomeFragment;
import com.demo_a_c.Page.PageActivity;
import com.demo_a_c.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InforActivity extends AppCompatActivity {
    ImageView back;
    TextView name, phone, mail;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infor);
        anhXa();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(Color.WHITE);

        sharedPreferences = getSharedPreferences("MyPreference", 0);
        name.setText(sharedPreferences.getString("name", ""));
        phone.setText(sharedPreferences.getString("phone", ""));
        mail.setText(sharedPreferences.getString("email", ""));

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    void anhXa() {
        name = findViewById(R.id.tv_i4_name);
        phone = findViewById(R.id.tv_i4_phone);
        mail = findViewById(R.id.tv_i4_mail);
        back = findViewById(R.id.back);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("if", "onDestroy: ");
    }
}