package com.demo_a_c.Login;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.demo_a_c.API.ApiService;
import com.demo_a_c.API.RetrofitClient;
import com.demo_a_c.ForgotPass.ForgotPassActivity;
import com.demo_a_c.Model.DoubleClickListener;
import com.demo_a_c.Model.LoginResponse;
import com.demo_a_c.Model.UserLoginRequest;
import com.demo_a_c.Page.PageActivity;
import com.demo_a_c.R;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    Button btn;
    TextView tv;
    TextView sdt, password;
    TextInputLayout tilUser, tilPassword;
    ProgressDialog dialog;
    public static String token = "";
    String MyPreference = "MyPreference";
    String getToken = "Token";
    SharedPreferences sharedPreferences;
    private long mLastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        anhXa();
        // milliseconds. the time that must pass before the user's click become valid


//        long currentTime = SystemClock.elapsedRealtime(); // not necessarily the current realworld time, and it doesn't matter. You can even use System.currentTimeMillis()
//        long elapsedTime = currentTime - mLastClickTime; // the time that passed after the last time the user clicked the button
//
//        if (elapsedTime < theUserCannotClickTime)
//            return; // 1000 milliseconds hasn't passed yet. ignore the click
//
//// over 1000 milliseconds has passed. do something with the click
//
//// record the time the user last clicked the button validly
//        mLastClickTime = currentTime;

        sharedPreferences = getSharedPreferences(MyPreference, 0);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(Color.WHITE);
        checkInput();

        View.OnClickListener buttonHandler = new View.OnClickListener() {
            public void onClick(View v) {
                // preventing double, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                    Log.d("zzzzzzzzzz", "onClick: ");
                }
                else {
                    //
                    loginForm();
                }
                mLastClickTime = SystemClock.elapsedRealtime();
            }
        };
        btn.setOnClickListener(buttonHandler);

//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                i++;
//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (i == 1) {
//                            loginForm();
//                        } else ;
//                        Log.d("zxc", "i: " + i);
//                        i = 0;
//                    }
//                }, 1000);
//            }
//        });
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, ForgotPassActivity.class));
            }
        });


    }

    void checkInput() {
        sdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (sdt.getText().toString().equals("")) {
                    tilUser.setHelperText("*Không được để trống email");
                } else tilUser.setHelperText("");
                ;
            }
        });

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (password.getText().toString().equals("")) {
                    tilPassword.setHelperText("*Không được để trống mật khẩu");
                } else tilPassword.setHelperText("");
            }
        });
    }

    void loginForm() {
        ApiService apiService = RetrofitClient.getRetrofit().create(ApiService.class);
        UserLoginRequest loginRequest = new UserLoginRequest(
                sdt.getText().toString(), password.getText().toString()
        );
        Call<LoginResponse> call = apiService.loginRequest(loginRequest);


        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (sdt.getText().toString().equals("") && password.getText().toString().equals("")) {
                    tilUser.setHelperText("Không được để trống email");
                    tilPassword.setHelperText("Không được để trống mật khẩu");
                } else if (sdt.getText().toString().equals("")) {
                    tilUser.setHelperText("Không được để trống email");
                } else if (password.getText().toString().equals("")) {
                    tilPassword.setHelperText("Không được để trống mật khẩu");
                } else if (response.body() == null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(LoginActivity.this, jObjError.getString("message"), Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    token = response.body().getData().getAccessToken();
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(getToken, token);
                    editor.commit();
                    Toast.makeText(LoginActivity.this, response.body().getMessage() + "", Toast.LENGTH_LONG).show();
                    finish();
                    startActivity(new Intent(LoginActivity.this, PageActivity.class));
                    dialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.d("err", "Fail: " + t);
                Log.d("err", "Fail: " + call);
            }
        });
        dialog.dismiss();
    }

    private void anhXa() {
        btn = findViewById(R.id.btn_login);
        tv = findViewById(R.id.tv_forgot_pass);
        sdt = findViewById(R.id.edt_sdt);
        password = findViewById(R.id.edt_password);
        tilUser = findViewById(R.id.til_username);
        tilPassword = findViewById(R.id.til_oldPass);
        dialog = new ProgressDialog(LoginActivity.this);
        sharedPreferences = getSharedPreferences(MyPreference, 0);
    }
}