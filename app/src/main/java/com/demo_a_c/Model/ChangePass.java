package com.demo_a_c.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangePass {
    public ChangePass(String newPass, String oldPass) {
        this.newPass = newPass;
        this.oldPass = oldPass;
    }

    @Expose
    @SerializedName("newPass")
    private String newPass;
    @Expose
    @SerializedName("oldPass")
    private String oldPass;

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    public String getOldPass() {
        return oldPass;
    }

    public void setOldPass(String oldPass) {
        this.oldPass = oldPass;
    }
}
