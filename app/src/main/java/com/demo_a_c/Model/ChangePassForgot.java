package com.demo_a_c.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangePassForgot {
    @Expose
    @SerializedName("deviceKey")
    private String deviceKey;
    @Expose
    @SerializedName("code")
    private String code;
    @Expose
    @SerializedName("password")
    private String password;
    @Expose
    @SerializedName("account")
    private String account;

    public ChangePassForgot(String deviceKey, String code, String password, String account) {
        this.deviceKey = deviceKey;
        this.code = code;
        this.password = password;
        this.account = account;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
