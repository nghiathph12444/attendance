package com.demo_a_c.Model;

import java.util.List;

public class GetList {
    String message;

    public GetList() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Attendance> getData() {
        return data;
    }

    public void setData(List<Attendance> data) {
        this.data = data;
    }

    public GetList(String message, List<Attendance> data) {
        this.message = message;
        this.data = data;
    }

    List<Attendance> data;




}
