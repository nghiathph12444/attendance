package com.demo_a_c.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Located {

    public Located() {
    }

    public Located(double lon, double lat) {
        this.lon = lon;
        this.lat = lat;
    }

    @Expose
    @SerializedName("lon")
    private double lon;
    @Expose
    @SerializedName("lat")
    private double lat;

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
