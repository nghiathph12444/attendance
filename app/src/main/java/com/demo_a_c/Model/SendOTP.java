package com.demo_a_c.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendOTP {
    public SendOTP(String account, String deviceKey) {
        this.account = account;
        this.deviceKey = deviceKey;
    }

    @Expose
    @SerializedName("account")
    private String account;
    @Expose
    @SerializedName("deviceKey")
    private String deviceKey;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }
}
