package com.demo_a_c.Model;

public class UserLoginRequest {
    private String username;
    private String passwordRequest;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordRequest() {
        return passwordRequest;
    }

    public void setPasswordRequest(String passwordRequest) {
        this.passwordRequest = passwordRequest;
    }

    public UserLoginRequest() {
    }

    public UserLoginRequest(String username, String passwordRequest) {
        this.username = username;
        this.passwordRequest = passwordRequest;
    }
}
