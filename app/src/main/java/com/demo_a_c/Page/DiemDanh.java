package com.demo_a_c.Page;

public class DiemDanh {
    String content, date, time, status;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DiemDanh() {
    }

    public DiemDanh(String content, String date, String time, String status) {
        this.content = content;
        this.date = date;
        this.time = time;
        this.status = status;
    }
}
