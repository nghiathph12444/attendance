package com.demo_a_c.Page;

import static android.content.Context.LOCATION_SERVICE;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.demo_a_c.API.ApiService;
import com.demo_a_c.API.RetrofitClient;
import com.demo_a_c.Model.Attendance;
import com.demo_a_c.Model.DoubleClickListener;
import com.demo_a_c.Model.EndlessRecyclerViewScrollListener;
import com.demo_a_c.Model.GetList;
import com.demo_a_c.Model.GetMassage;
import com.demo_a_c.Model.Located;
import com.demo_a_c.Model.LoginResponse;
import com.demo_a_c.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements LocationListener {
    Button btn_dd;
    RecyclerView rcv;
    HomeRecyclerAdapter adapter;
    List<Attendance> list;
    ProgressDialog progress;
    LocationManager locationManager;
    LinearLayoutManager layoutManager;
    double mLong, mLat;
    private long mLastClickTime = 0;
    EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    int i = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d("zxc", "createView: " + "Fragment");
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btn_dd = view.findViewById(R.id.btn_dd);
        rcv = view.findViewById(R.id.rcv);
        progress = new ProgressDialog(getContext());
        layoutManager = new LinearLayoutManager(getContext());

        getList(0);
        setLocation();
        getProfile();

        View.OnClickListener buttonHandler = new View.OnClickListener() {
            public void onClick(View v) {
                // preventing double, using threshold of 1000 ms
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                } else {
                    //
                    progress.show();
                    if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{
                                Manifest.permission.ACCESS_FINE_LOCATION
                        }, 100);

                    } else {
                        attendance(mLong, mLat);
                        progress.dismiss();
                        getList(0);

                        rcv.addOnScrollListener(endlessRecyclerViewScrollListener);

                    }
                    progress.dismiss();
                }
                mLastClickTime = SystemClock.elapsedRealtime();
            }
        };
        btn_dd.setOnClickListener(buttonHandler);
//
    }


    private void getList(int page) {
        if (page == 0) {
            progress.show();
        } else ;
        ApiService apiService = RetrofitClient.getRetrofit().create(ApiService.class);
        Call<GetList> call = apiService.getList(i);
        call.enqueue(new Callback<GetList>() {
            @Override
            public void onResponse(Call<GetList> call, Response<GetList> response) {
                if (response.body() == null) {
                    Log.d("zzz", "null");

                } else {
                        if (page == 0) {
                            list = new ArrayList<>();
                            list.addAll (response.body().getData());
                            adapter = new HomeRecyclerAdapter(list, getContext());
                            rcv.setAdapter(adapter);
                            rcv.setLayoutManager(layoutManager);
                            endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener((LinearLayoutManager) rcv.getLayoutManager()) {
                                @Override
                                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                                    getList(page);
                                }
                            };
                            rcv.addOnScrollListener(endlessRecyclerViewScrollListener);
                        } else {
                            list.addAll(response.body().getData());
                            adapter.notifyDataSetChanged();
                            Log.d("acb", "pages: " + i);
                        }



                    adapter.notifyDataSetChanged();
                }

                progress.dismiss();
            }

            @Override
            public void onFailure(Call<GetList> call, Throwable t) {
                Toast.makeText(getContext(), "Kiểm tra kết nối", Toast.LENGTH_SHORT).show();
                progress.dismiss();
                progress.cancel();
            }
        });

    }

    private void attendance(Double lon, Double lat) {
        ApiService apiService = RetrofitClient.getRetrofit().create(ApiService.class);
        Call<GetMassage> call = apiService.attendance(new Located(lon, lat));
        call.enqueue(new Callback<GetMassage>() {
            @Override
            public void onResponse(Call<GetMassage> call, Response<GetMassage> response) {
                if (response.body() == null) {

                } else ;
                Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<GetMassage> call, Throwable t) {

            }
        });
    }

    void setLocation() {
        locationManager = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1, this);
    }

    private void getProfile() {
        ApiService apiService = RetrofitClient.getRetrofit().create(ApiService.class);
        Call<LoginResponse> call = apiService.getProfile();
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.body() == null) {
//
                } else {
                    SharedPreferences.Editor editor = getContext().getSharedPreferences("MyPreference", 0).edit();
                    editor.putString("name", response.body().getData().getName());
                    editor.putString("phone", response.body().getData().getPhone());
                    editor.putString("email", response.body().getData().getEmail());
                    editor.commit();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        mLong = location.getLongitude();
        mLat = location.getLatitude();
        Log.d("abc1", "location: " + mLong + "_" + mLat);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {

    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {

    }

}
