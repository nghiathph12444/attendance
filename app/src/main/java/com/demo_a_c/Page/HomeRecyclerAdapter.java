package com.demo_a_c.Page;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demo_a_c.API.ApiService;
import com.demo_a_c.API.BaseObject;
import com.demo_a_c.API.RetrofitClient;
import com.demo_a_c.Model.Attendance;
import com.demo_a_c.Model.GetList;
import com.demo_a_c.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.List;

public class HomeRecyclerAdapter extends RecyclerView.Adapter<HomeRecyclerAdapter.HomeHolder> {
    List<Attendance> list;
    Context context;
    String formattedTime ="";
    public HomeRecyclerAdapter(List<Attendance> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public HomeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        HomeHolder holder;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_history_, parent, false);
        holder = new HomeHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeHolder holder, int position) {
        Attendance  attendance = list.get(position);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZ");
        SimpleDateFormat output = new SimpleDateFormat("dd/MM' | 'HH:mm");
        try {
            Log.d("zzz", "Time: "+ attendance.getCreatedAt());
            Date d = sdf.parse(attendance.getCreatedAt());
            formattedTime = output.format(d);
            holder.tv_time.setText(output.format(d) +"");
            holder.tv_content.setText(attendance.getNote());
            if (attendance.getStatus() ==1){
                holder.tv_status.setText("Success");
            }else {
                holder.tv_status.setText("Fail");
                holder.tv_status.setTextColor(Color.RED);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    class HomeHolder extends RecyclerView.ViewHolder {
        TextView tv_content, tv_time, tv_status;
        public HomeHolder(@NonNull View itemView) {
            super(itemView);
            tv_content = itemView.findViewById(R.id.tv_home_note);
            tv_time = itemView.findViewById(R.id.tv_home_time);
            tv_status = itemView.findViewById(R.id.tv_home_status);
        }
    }
}
