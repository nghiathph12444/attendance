package com.demo_a_c.Page;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.demo_a_c.API.ApiService;
import com.demo_a_c.API.RetrofitClient;
import com.demo_a_c.Login.LoginActivity;
import com.demo_a_c.Model.LoginResponse;
import com.demo_a_c.R;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PageActivity extends AppCompatActivity{
    BottomNavigationView bnv;
    SharedPreferences sharedPreferences;
    ViewPagerAdapter adapter;
    ViewPager2 vp2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page);
        sharedPreferences = getSharedPreferences("MyPreference", 0);
        bnv = findViewById(R.id.bnv);
        vp2 = findViewById(R.id.vp2);
        adapter  = new ViewPagerAdapter(this);
        vp2.setAdapter(adapter);
        getWindow().setStatusBarColor(Color.WHITE);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
//
//        getSupportFragmentManager().beginTransaction()
//                .add(R.id.fcv, HomeFragment.class, null)
//                .commit();
        vp2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                switch (position){
                    case 0:
                        bnv.getMenu().findItem(R.id.mnu_home).setChecked(true);
                        break;
                    case 1:
                        bnv.getMenu().findItem(R.id.mnu_acc).setChecked(true);
                        break;
                }
            }
        });
        bnv.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.mnu_home:
                        vp2.setCurrentItem(0);
                        break;
                    case R.id.mnu_acc:
                        vp2.setCurrentItem(1);
                        break;
                }
                return true;
            }
        });

    }




}