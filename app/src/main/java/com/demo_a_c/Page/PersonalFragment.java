package com.demo_a_c.Page;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;

import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.demo_a_c.API.ApiService;
import com.demo_a_c.API.RetrofitClient;
import com.demo_a_c.ChangePassword.ChangePasswordActivity;
import com.demo_a_c.Information.InforActivity;
import com.demo_a_c.Model.LoginResponse;
import com.demo_a_c.R;
import com.demo_a_c.Splat.SplatActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonalFragment extends Fragment {
    ImageView avt;
    TextView changePass, tv_phone, tv_name, tvLogout;
    ProgressDialog progressDialog;
    ConstraintLayout cl;
    SharedPreferences sharedPreferences;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_personal, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getContext().getSharedPreferences("MyPreference", 0);
        Log.d("abc", "token: "+sharedPreferences.getString("Token",""));
//        getProfile();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        avt = view.findViewById(R.id.img_avt);
        changePass = view.findViewById(R.id.tv_change_pass);
        tv_phone = view.findViewById(R.id.tv_person_phone);
        tv_name = view.findViewById(R.id.tv_person_name);
        tvLogout = view.findViewById(R.id.tv_logout);
        cl = view.findViewById(R.id.constraintLayout);


        progressDialog = new ProgressDialog(getContext());
        sharedPreferences = getContext().getSharedPreferences("MyPreference", 0);
        tv_name.setText(sharedPreferences.getString("name", ""));
        tv_phone.setText(sharedPreferences.getString("phone", ""));
        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
//
            }
        });
        cl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), InforActivity.class));
            }
        });
        changePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ChangePasswordActivity.class));
            }
        });
    }
    private void showDialog(){
        Dialog dialog = new Dialog(getContext());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_logout);
        TextView yes, no;
        yes = dialog.findViewById(R.id.tv_yes);
        no = dialog.findViewById(R.id.tv_no);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Token", "");
                editor.commit();
                getActivity().finishAffinity();
                startActivity(new Intent(getContext(), SplatActivity.class));
                progressDialog.show();
                progressDialog.dismiss();
            }
        });
        dialog.show();
    }
    private void getProfile(){
        ApiService apiService = RetrofitClient.getRetrofit().create(ApiService.class);
        Call<LoginResponse> call = apiService.getProfile();
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.body()==null);
                else {
//                    SharedPreferences.Editor editor = getContext().getSharedPreferences("MyPreference", 0).edit();
//                    editor.putString("name", response.body().getData().getName());
//                    editor.putString("phone", response.body().getData().getPhone());
//                    editor.putString("email", response.body().getData().getEmail());
//                    editor.commit();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

            }
        });
    }

}
