package com.demo_a_c.Splat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.demo_a_c.BroadCast.MyBroadCastReceiver;
import com.demo_a_c.Login.LoginActivity;
import com.demo_a_c.Page.PageActivity;
import com.demo_a_c.R;

public class SplatActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    public static String checkToken = "";
    boolean check;
//    private BroadcastReceiver broadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splat);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        getWindow().setStatusBarColor(Color.WHITE);
//        broadcastReceiver = new MyBroadCastReceiver();

        sharedPreferences = getSharedPreferences("MyPreference", 0);
        checkToken = sharedPreferences.getString("Token", "");

//        registerNetworkBroadcastForNougat();

            if (checkToken.equals("")){
                finish();
                startActivity(new Intent(getBaseContext(), LoginActivity.class));
            }
            else {
                startActivity(new Intent(getBaseContext(), PageActivity.class));
                finish();
            }

        Log.d("checkwif", "checkWifi: " +check);

    }
//    private void registerNetworkBroadcastForNougat() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            registerReceiver(broadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//        }
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            registerReceiver(broadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
//        }
//    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

}